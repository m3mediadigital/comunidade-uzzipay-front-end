import React from 'react';
import Avatar from '@material-ui/core/Avatar';

const _Avatar = ({name, file}) => {
    return (
        <div className="d-flex align-items-center avatar">
            <Avatar alt="Avatar" src={file} />
            <span className="pl-2">{name}</span>
        </div>
    );
}

export default _Avatar