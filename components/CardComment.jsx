import { Col, Row, Nav, Card } from 'react-bootstrap'
import Avatar from '@material-ui/core/Avatar';
import Image from 'next/image'
import Link from 'next/link'

const _CARDCOMMENT = () =>{
    return(
        <Row className="posts-list">
            <Col xs={12} md={1} className="d-flex justify-md-content-center">
                <Link href="/m3">
                    <Avatar alt="Avatar" src="images/avatar.jpg" className="w-100 d-none d-md-block" />
                </Link>           
                <Row className="d-md-none">
                    <Col xs={4}>
                        <Link href="/m3">
                            <Avatar alt="Avatar" src="images/avatar.jpg" className="w-100" />
                        </Link>
                    </Col>
                    <Col>
                        <small className="d-md-none">Feliipe Mendonça</small>
                        <br/>
                        <small className="d-md-none">10min</small>
                    </Col>
                </Row>
                
            </Col>
            <Col xs={12} md={10}>
                <small className="d-none d-md-block pt-3">Feliipe Mendonça</small>
                <small className="d-none d-md-block pt-1">10min</small>
                <Card className="w-100 posts-card pt-4 pb-4 mt-md-3 border-0">
                    <Row className="pl-md-5 pr-md-5 pl-2 pr-2 pl-lg-5 pr-lg-5">
                        <Col sm={12}>
                            <Card.Text>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo magni architecto nam tempora earum 
                                ratione labore sit, sequi, dignissimos deserunt eum
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo magni architecto nam tempora earum 
                                ratione labore sit, sequi, dignissimos deserunt eum
                            </Card.Text>
                        </Col>
                        <Col sm={12}>
                            <Nav className="flex-row align-items-center likes pt-3" activeKey="/home">
                                <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                                    <Nav.Link href="/home" className="d-flex p-2">
                                        <div className="next-image icons">
                                            <Image src="/images/icons/curti-black.svg" layout="fill" />
                                        </div>  
                                        <span className="pl-2">435</span>
                                    </Nav.Link>
                                </Nav.Item>
                                <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                                    <Nav.Link href="/home" className="d-flex p-2">
                                        <div className="next-image icons">
                                            <Image src="/images/icons/comment-black.svg" layout="fill" />
                                        </div>  
                                        <span className="pl-2">98</span>
                                    </Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Col>
                    </Row>
                </Card>
            </Col>
        </Row>
    )
}

export default _CARDCOMMENT