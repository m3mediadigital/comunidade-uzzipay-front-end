import { Card, Col, Row } from "react-bootstrap"
import Likes from './likes'
import Link from 'next/link'

const _CARDPOST = ({ item }) => {
    return (
        <Card className="w-100 posts-card pt-4 pb-4 mb-4 border-0">
            <Row className="pl-md-5 pr-md-5 pl-2 pr-2 pl-lg-5 pr-lg-5">
                <Link href="/posts/[slug]" as={`/posts/${item.slug}`}>
                    <Col sm={12} lg={6} xl={7}>
                        <Card.Title>{item.title}</Card.Title>
                        <Card.Text>
                            {item.content}
                        </Card.Text>
                    </Col>
                </Link>
                <Col sm={12} lg={6} xl={5} className="d-flex align-items-center justify-lg-content-end pt-4 pt-lg-0">
                    <Likes item={item.user}/>
                </Col>
            </Row>
        </Card>
    )
}

export default _CARDPOST