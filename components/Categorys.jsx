// import React, {Component} from 'react'
import { Card, Nav } from 'react-bootstrap';
import Link from 'next/link'

const _CATEGORYS = (props) => {
    const { category } = props
    return (
        <>
            <Card className="categorys-content-tabs-content-card w-100 border-0">
                <Card.Body className="p-3 pt-0">
                    <Nav className="flex-row align-items-center justify-content-between pt-3 pb-3" activeKey="/home">
                        <Nav.Item>
                            <h4><strong>{category.get('title')}</strong></h4>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link className="nav-link" href={`/categoria/${category.get('slug')}`}>
                                Ver mais
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                    <Nav className="d-block categorys-card-content pb-3" activeKey="/home">
                        {
                            category.get('post').map((item, index) => {
                                return (
                                    <Nav.Item key={index}>
                                        <Nav.Link href={`/posts/${item.get('slug')}`} className="d-flex p-2">
                                            {item.get('title')}
                                        </Nav.Link>
                                    </Nav.Item>
                                )
                            })
                        }
                    </Nav>
                </Card.Body>
            </Card>
        </>
    )
}

export default _CATEGORYS;