import { Col, Form, Row, Button } from 'react-bootstrap'
import _CARDCOMMENT from './CardComment'
import Avatar from '@material-ui/core/Avatar';
import Link from 'next/link'

const _COMMENTS = () => {
    return(
        <div className="posts-intern-comments">
            <p className="pb-3">Comentários</p>
            <Row>
                <Col xs={2} sm={2} md={1} className="d-flex justify-content-center">
                    <Link href="/m3">
                        <Avatar alt="Avatar" src="images/avatar.jpg" className="w-100" />
                    </Link>
                </Col>
                <Col xs={10} sm={10} md={11}>
                    <Form className="pb-5">
                        <Form.Control as="textarea" className="rounded-0" rows={5} />
                        <Button variant="primary" className="border-0 position-absolute pt-2 pb-2 pr-5 pl-5 rounded-0" type="submit">
                            Comentar
                        </Button>
                    </Form>
                </Col>
                <Col sm={12} className="pt-5 pb-5">
                    <_CARDCOMMENT />
                    <Col sm={12} className="pl-5 pt-5 pr-5">
                        <div className="pl-md-5 pb-5 pr-md-5">
                            <_CARDCOMMENT />
                            <_CARDCOMMENT />
                            <_CARDCOMMENT />
                            <_CARDCOMMENT />
                            <_CARDCOMMENT />
                            <_CARDCOMMENT />
                        </div>                    
                    </Col>

                    <_CARDCOMMENT />
                    <Col sm={12} className="pl-5 pt-5 pr-5">
                        <div className="pl-md-5 pb-5 pr-md-5">
                            <_CARDCOMMENT />
                            <_CARDCOMMENT />
                            <_CARDCOMMENT />
                        </div>                    
                    </Col>
                    <_CARDCOMMENT />
                    <_CARDCOMMENT />
                </Col>
            </Row>
        </div>
    )
}

export default _COMMENTS