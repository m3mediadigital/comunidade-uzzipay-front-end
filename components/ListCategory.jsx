import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import Categorys from './Categorys'
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}

const ListCategorys = (props) =>{

    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    console.log(props.categorys)

    return (
        <section className="categorys container border-0 pt-4">
            <AppBar position="static" color="default" className="pb-2">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                    className="categorys-content"
                >
                    <Tab label="categoria" className="categorys-content-tabs text-uppercase" {...a11yProps(0)} />
                    <Tab label="recentes" className="categorys-content-tabs text-uppercase" {...a11yProps(1)} />
                    <Tab label="melhores" className="categorys-content-tabs text-uppercase" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            
            <TabPanel value={value} className="categorys-content-tabs-content p-0" index={0}>
                {
                    props.categorys.map((item, key) => {
                        return(
                            <Categorys category={item} posts={item.get('post')} />
                        )
                    })
                }
            </TabPanel>
            
            
            <TabPanel value={value} className="categorys-content-tabs-content p-0" index={1}>
                {/* <Categorys /> */}
            </TabPanel>
            <TabPanel value={value} className="categorys-content-tabs-content p-0" index={2}>
                {/* <Categorys /> */}
            </TabPanel>
        </section>
    );
}

export default ListCategorys