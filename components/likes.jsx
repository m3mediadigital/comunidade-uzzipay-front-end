import { Nav } from "react-bootstrap"
import Avatar from './Avatar'
import Image from 'next/image'

const _LIKES = ({item}) =>{
    return(
        <Nav className="flex-row align-items-center likes" activeKey="/home">
            <Nav.Item className="d-flex align-content-center">
                <Nav.Link href="/m3" className="d-flex align-items-center pl-0 pb-2 pr-3">
                    <Avatar name={item.name} file={item.file.path} />
                </Nav.Link>
            </Nav.Item>
            <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                <Nav.Link href="/post" className="d-flex p-2">
                    <div className="next-image icons">
                        <Image src="/images/icons/curti-black.svg" layout="fill" />
                    </div>
                    <span className="pl-2">435</span>
                </Nav.Link>
            </Nav.Item>
            <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                <Nav.Link href="/post" className="d-flex p-2">
                    <div className="next-image icons">
                        <Image src="/images/icons/comment-black.svg" layout="fill" />
                    </div>
                    <span className="pl-2">98</span>
                </Nav.Link>
            </Nav.Item>
            <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                <Nav.Link href="/post" className="d-flex p-2">
                    <div className="next-image icons">
                        <Image src="/images/icons/data.svg" layout="fill" />
                    </div>
                    <span className="pl-2">Hoje</span>
                </Nav.Link>
            </Nav.Item>
        </Nav>
    )
}

export default _LIKES