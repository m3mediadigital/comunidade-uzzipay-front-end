import { Card, Col, Row, Nav,  } from "react-bootstrap"

import Link from 'next/link'
import Image from 'next/image'

const _CardPost = () =>{
    return(
        <Card className="w-100 posts-card pt-4 pb-4 mb-4 border-0">
            <Row className="pl-md-5 pr-md-5 pl-2 pr-2 pl-lg-5 pr-lg-5">
                <Link href="./post">
                    <Col sm={12} >
                        <Card.Title>Como faço para abrir minha conta?</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo magni architecto nam tempora earum 
                            ratione labore sit, sequi, dignissimos deserunt eum
                        </Card.Text>
                    </Col>
                </Link>
                <Col sm={12} className="d-flex align-items-center justify-lg-content-end pt-4">
                    <Nav className="flex-row align-items-center likes" activeKey="/home">
                        <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                            <Nav.Link href="/home" className="d-flex p-2">
                                <div className="next-image icons">
                                    <Image src="/images/icons/curti-black.svg" width={14} height={0} />
                                </div>
                                <span className="pl-2">435</span>
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                            <Nav.Link href="/home" className="d-flex p-2">
                                <div className="next-image icons">
                                    <Image src="/images/icons/comment-black.svg" width={14} height={0} />
                                </div>
                                <span className="pl-2">98</span>
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3">
                            <Nav.Link href="/home" className="d-flex p-2">
                                <div className="next-image icons">
                                    <Image src="/images/icons/data.svg" width={14} height={0} />
                                </div>
                                <span className="pl-2">Hoje</span>
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Col>
            </Row>
        </Card>
    )
}

export default _CardPost