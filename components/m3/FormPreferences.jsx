import React from 'react'
import {Col, Form, Button } from 'react-bootstrap'
import Image from 'next/image'
import { useRouter } from 'next/router'

const _Form = () => {
    const router = useRouter()

    return (
        <Form className="admin-form" method="post">
            <Form.Row>
                <Col sm={12} md={6} className="pb-2">
                    <Form.Group>
                        <Form.Label>Nome</Form.Label>
                        <Form.Control type="text" value="Marquinhos Gomes" name="name" />
                    </Form.Group>
                </Col>
                <Col sm={12} md={6} className="pb-2">
                    <Form.Group>
                        <Form.Label>E-mail</Form.Label>
                        <Form.Control type="text" value="email@email.com" name="email" />
                    </Form.Group>
                </Col>
                <Col sm={12} md={4} className="pb-2">
                    <Form.Group>
                        <Form.Label>Usuário</Form.Label>
                        <Form.Control type="text" value="marquinhosgomes" name="user" />
                    </Form.Group>
                </Col>
                <Col sm={12} md={4} className="pb-2">
                    <Form.Group>
                        <Form.Label>Senha</Form.Label>
                        <Form.Control type="password" value="" name="password" />
                    </Form.Group>
                </Col>
                <Col sm={12} md={4} className="pb-2">
                    <Form.Group>
                        <Form.Label>Repetir Senha</Form.Label>
                        <Form.Control type="password" value="" name="confirm_password" />
                    </Form.Group>
                </Col>
                <Col sm={12} md={4} className="pb-2 d-flex justify-content-center align-items-center">
                    <div className="next-image">
                        <Image src="/images/avatar.jpg" className="avatar" layout="fill" />
                    </div>
                </Col>
                <Col sm={12} md={8} className="pb-2">
                    <Form.Group>
                        <Form.Label>Sobre</Form.Label>
                        <Form.Control as="textarea" rows={5} name="about" className="pt-5" value="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsam soluta atque velit inventore officia quisquam perspiciatis, nostrum nam placeat natus. Nisi repudiandae dolorum cupiditate, voluptatem nemo tempora numquam quis? Reprehenderit!" />
                    </Form.Group>
                </Col>
                <Col sm={12} md={4} className="pb-2">
                    <Form.Group>
                        <Form.Label>Facebook</Form.Label>
                        <Form.Control type="text" name="facebook" value="Marquinhos Gomes" />
                    </Form.Group>
                </Col>
                <Col sm={12} md={4} className="pb-2">
                    <Form.Group>
                        <Form.Label>Instaram</Form.Label>
                        <Form.Control type="text" name="instagram" value="marquinhosgomes" />
                    </Form.Group>
                </Col>
                <Col sm={12} className="pt-3">
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsam soluta atque velit inventore officia quisquam perspiciatis, nostrum nam placeat natus. Nisi repudiandae dolorum cupiditate, voluptatem nemo tempora numquam quis? Reprehenderit!</p>
                </Col>
                <Col sm={12} className="d-flex justify-content-end">
                    <Button variant="light" type="button" onClick={() => router.push('/m3')}>Salvar Alterações</Button>
                </Col>
            </Form.Row>  
        </Form>
    )
}

export default _Form