import React from 'react'
import { Col, Row, Container, Nav } from 'react-bootstrap'
import Image from 'next/image'

const _Header = () =>{

    return(
        <section className="admin">
            <Container className="pt-5 pt-md-0 pr-lg-4 pl-lg-4 pt-lg-2 pb-lg-2">
                <Row>
                    <Col sm={12} md={3} lg={3} className="d-flex justify-content-center align-items-center">
                        <div className="next-image">
                            <Image src="/images/avatar.jpg" className="avatar" layout="fill" />
                        </div>
                    </Col>
                    <Col sm={12} md={9} lg={9} className="pt-5 pb-5 admin-content d-flex align-items-center justify-content-center justify-content-md-start text-center text-md-left">
                        <Row className="p-0 w-100">
                            <Col sm={12} md={7} lg={6}>
                                <h2 className="text-uppercase m-0">Marquinhos gomes</h2>
                                <p>@marquinhosgomes</p>
                                <p className="text-uppercase">10 seguindo | 300 seguidores</p>
                            </Col>
                            <Col sm={12} md={5} lg={6} className="d-flex justify-content-center justify-content-lg-end">
                                <Nav className="flex-row align-items-center likes" activeKey="/home">
                                    <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2">
                                        <Nav.Link href="./m3/preferencias" className="d-flex p-2">
                                            <div className="next-image icons">
                                                <Image src="/images/icons/settings.svg" layout="fill" />
                                            </div>
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2">
                                        <Nav.Link href="/m3/posts/create" className="d-flex p-2">
                                            <div className="next-image icons">
                                                <Image src="/images/icons/messages.svg" layout="fill" />
                                            </div>
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item className="d-flex align-content-center pl-0 pl-lg-2">
                                        <Nav.Link href="/home" className="d-flex p-2">
                                            <div className="next-image icons">
                                                <Image src="/images/icons/posts.svg" layout="fill" />
                                            </div>
                                        </Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </Col>
                            <Col sm={12} className="admin-content-about text-justify">
                                <p><strong>SOBRE</strong></p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur consequuntur aperiam, a nulla tempore at vel qui molestiae quis. Recusandae amet enim iste earum nesciunt sint nam dignissimos dicta cum.</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}

export default _Header