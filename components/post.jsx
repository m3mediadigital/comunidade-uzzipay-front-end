import { Container, Image } from 'react-bootstrap'

import Like from './likes'
import Commnets from './Commnets'
import CartPost from './CardPost'

const _post = ({ item }) => {
    const { file } = item.has_file
    return (

        <section className="posts">
            <Container className="posts-intern">
                <div className="pt-5 pb-4">
                    <h3><strong>{item.title}</strong></h3>
                </div>
                {/* <Like item={item.user} /> */}
                <div className="hr pt-4 pb-4">
                    <hr />
                </div>
                <figure className="figure">
                    <Image bsPrefix="img" src="/images/post.jpg" className="img-fluid" />
                    {/* <figcaption className="figure-caption pt-1">
                        <i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</i>
                    </figcaption> */}
                </figure>

                <div className="posts-intern-text pt-5" dangerouslySetInnerHTML={{ __html: item.content }}></div>
                <div className="hr pt-4 pb-4">
                    <hr />
                </div>
                <Commnets />
                <div className="hr pt-4 pb-4">
                    <hr />
                </div>
                <div className="posts">
                    <div className="posts-list">
                        <p className="pb-3">Veja também</p>
                        {/* {
                            item.category.post.map((item, key) => (
                                // <CartPost item={item} key={key} />
                            ))
                        }  */}
                    </div>
                </div>
            </Container>
        </section>
    )
}

export default _post;