import React, {Component} from 'react'
import { Container, Card, Row, Col, Nav, Image, Carousel } from 'react-bootstrap';
import Avatar from './Avatar'
import { useRouter } from 'next/router'

const _postOne = (data) => {
    const router = useRouter()
    return (
        <Card className="w-100 border-0 posts-card posts-card-one" style={{ backgroundImage: 'url(images/postOne.png)' }} onClick={() => router.push('/post')}>
            <Card.Body className="pt-5 w-100">
                <div className="card-body-content position-absolute">
                    <Card.Title className="w-75">
                        <strong>Comunidade Uzzipay</strong>
                    </Card.Title>
                    <Card.Text>
                        lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                    </Card.Text>
                    <Nav className="flex-row align-items-center" activeKey="/home">
                        <Nav.Item className="d-flex align-content-center">
                            <Nav.Link href="/m3" className="d-flex align-items-center pl-0 pt2 pb-2 pr-3">
                                <Avatar />
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="d-flex align-content-center">
                            <Nav.Link href="/post" className="d-flex p-2">
                                <Image bsPrefix="img" src="images/icons/curti.svg" className="icons"/>
                                <span className="pl-2">435</span>
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="d-flex align-content-center">
                            <Nav.Link href="/post" className="d-flex p-2">
                                <Image bsPrefix="img" src="images/icons/comment.svg" className="icons"/>
                                <span className="pl-2">98</span>
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                </div>
            </Card.Body>
        </Card>        
    )
}

const _postTwo = () => {
    return (
         <Card className="w-100 border-0 posts-card posts-card-two mb-3" style={{ backgroundImage: 'url(images/postTwo.jpg)' }}>
            <Card.Body className="pt-5 w-100">
                <div className="card-body-content position-absolute">
                    <Card.Title className="w-75">
                        <strong>Como faço para abrir minha conta?</strong>
                    </Card.Title>
                    <Nav className="flex-row align-items-center" activeKey="/home">
                        <Nav.Item className="d-flex align-content-center">
                            <Nav.Link href="/m3" className="d-flex align-items-center pl-0 pt2 pb-2 pr-3">
                                <Avatar />
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="d-flex align-content-center">
                            <Nav.Link href="/post" className="d-flex p-2">
                                <Image bsPrefix="img" src="images/icons/curti.svg" className="icons"/>
                                <span className="pl-2">435</span>
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="d-flex align-content-center">
                            <Nav.Link href="/post" className="d-flex p-2">
                                <Image bsPrefix="img" src="images/icons/comment.svg" className="icons"/>
                                <span className="pl-2">98</span>
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                </div>
            </Card.Body>
        </Card>          
    )
}


class _POSTS extends Component{
    render(){
        return(
            <section className="posts">
                <Container className="d-none d-lg-block">
                    <Row>
                        <Col sm={12} lg={6}>
                            <_postOne />
                        </Col>
                        <Col sm={12} lg={6}>
                            <Row>
                                <Col sm={12}>
                                    <_postTwo />
                                </Col>
                                <Col>
                                    <_postTwo />
                                </Col>
                            </Row>
                        </Col>
                    </Row>            
                </Container>
                <Carousel className="d-lg-none">
                    <Carousel.Item interval={4000}>
                        <_postOne />
                    </Carousel.Item>
                    <Carousel.Item interval={4000}>
                        <_postOne />
                    </Carousel.Item>
                    <Carousel.Item interval={4000}>
                        <_postOne />
                    </Carousel.Item>
                    <Carousel.Item interval={4000}>
                        <_postOne />
                    </Carousel.Item>
                </Carousel>
            </section>
        )
    }
}


export default _POSTS