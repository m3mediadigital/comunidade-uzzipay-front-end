import React, {Component} from 'react'

import Head from './head'
import Header from './header'
import Footer from './footer'

class App extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <React.Fragment>
                <Head  {... this.props} />
                <Header />
                {this.props.children}
                <Footer />
            </React.Fragment>
        )
    }
}

export default App