import { Navbar, Nav, Container } from 'react-bootstrap';
import Image from 'next/image'

const _FOOTER = (props) => {
    return(
        <footer className="pt-4 pt-lg-5 pb-3 position-absolute w-100">
            <Container className="d-none d-lg-block">
                <Navbar>
                    <Nav className="justify-content-between flex-row w-100" activeKey="/home">
                        <Nav.Item>
                            <Nav className="mr-auto">
                                <Nav.Link href="/home" className="p-0">
                                    <div className="next-image">
                                        <Image src="/images/icons/logo.svg" layout="fill" />
                                    </div> 
                                </Nav.Link>
                                <Nav.Link href="#home" className="pl-5">Todos os direitos reservado</Nav.Link>
                            </Nav>
                        </Nav.Item>
                        <Nav.Item className="social">
                            <Nav className="mr-auto">
                                <Nav.Link href="#home">
                                    <div className="next-image">
                                        <Image src="/images/icons/facebook.svg" layout="fill" />
                                    </div> 
                                </Nav.Link>
                                <Nav.Link href="#home">
                                    <div className="next-image">
                                        <Image src="/images/icons/twitter.svg" layout="fill" />
                                    </div> 
                                </Nav.Link>
                                <Nav.Link href="#home">
                                    <div className="next-image">
                                        <Image src="/images/icons/instagram.svg" layout="fill" />
                                    </div> 
                                </Nav.Link>
                                <Nav.Link href="#home">
                                    <div className="next-image">
                                        <Image src="/images/icons/youtube.svg" layout="fill" />
                                    </div> 
                                </Nav.Link>
                            </Nav>                
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link href="https://www.novam3.com.br">
                                <strong>Nova M3</strong>
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Navbar>
            </Container>
            <Container className="d-lg-none">
                <Nav className="justify-content-between w-100">
                    <Nav.Item>
                        <Nav.Link href="/home" className="p-0 pt-3">
                            <div className="next-image">
                                <Image src="/images/icons/logo.svg" layout="fill" />
                            </div> 
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav className="mr-auto justify-content-between">
                            <Nav.Link href="#home" className="p-2">
                                <div className="next-image">
                                    <Image src="/images/icons/facebook.svg" layout="fill" />
                                </div> 
                            </Nav.Link>
                            <Nav.Link href="#home" className="p-2">
                                <div className="next-image">
                                    <Image src="/images/icons/twitter.svg" layout="fill" />
                                </div>
                            </Nav.Link>
                            <Nav.Link href="#home" className="p-2">
                                <div className="next-image">
                                    <Image src="/images/icons/instagram.svg" layout="fill" />
                                </div>
                            </Nav.Link>
                            <Nav.Link href="#home" className="p-2">
                                <div className="next-image">
                                    <Image src="/images/icons/youtube.svg" layout="fill" />
                                </div>
                            </Nav.Link>
                        </Nav>
                    </Nav.Item>
                </Nav>
                <Nav className="mr-auto justify-content-between w-100 pt-2">
                    <Nav.Item>
                        <Nav.Link href="#home" className="pl-0">Todos os direitos reservado</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="https://www.novam3.com.br">
                            <strong>Nova M3</strong>
                        </Nav.Link>
                    </Nav.Item>
                </Nav>
            </Container>
        </footer>
    )
}

export default _FOOTER