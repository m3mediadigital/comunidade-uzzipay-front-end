import Head from 'next/head'
import { useRouter } from 'next/router'

const _HEAD = (props) => {
    const router = useRouter()
    return (
        <>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta name="description" content={props.description ?? 'Descrição'} />
                <meta name="keywords" content={props.keywords ?? 'keywords'} />

                <meta name="robots" content="index,follow,noodp" />
                <meta name="googlebot" content="index,follow" />

                <meta name="subjet" content={props.subjet ?? 'subjet'} />
                <meta name="abstract" content={props.abstract ?? 'abstract'} />
                <meta name="topic" content={props.topic ?? 'topic'} />
                <meta name="summary" content={props.summary ?? 'summary'} />

                {/* Facebook */}
                {/* <meta property="og:url" content={router.route} /> */}
                <meta property="og:url" content="http://localhost:3000" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={props.title ?? 'title'} />
                <meta property="og:image" content={props.image ?? 'Images'} />
                <meta property="og:description" content={props.description ?? 'description'} />
                <meta property="og:site_name" content="Comunidade UzziPay" />
                <meta property="og:locale" content="pt_BR" />

                {/* <link href="https://plus.google.com/+YourPage" rel="publisher" /> */}
                <meta itemprop="name" content={props.title ?? 'title'} />
                <meta itemprop="description" content={props.description ?? 'description'} />
                <meta itemprop="image" content={props.image ?? 'image'} />

                {/* Twitter */}
                <meta name="twitter:card" content="summary" />
                {/* <meta name="twitter:site" content="@yield('TWITTER_ACCOUNT', env('TWITTER_ACCOUNT'))" /> */}
                {/* <meta name="twitter:creator" content="@individual_account" /> */}
                {/* <meta name="twitter:url" content={router.route} /> */}
                <meta name="twitter:url" content="http://localhost:3000" />
                <meta name="twitter:title" content={props.title ?? 'title'} />
                <meta name="twitter:description" content={props.description ?? 'description'} />
                <meta name="twitter:image" content={props.image ?? 'image'} />

                <title>{ props.title ? 'Comunidade UzziPay - ' + props.title : 'Comunidade UzziPay' }</title>
            </Head>
        </>
    )
}

export default _HEAD