import React, { useState, useCallback } from 'react'
import { HamburgerSqueeze } from 'react-animated-burgers'
import { Navbar, Nav, Container } from 'react-bootstrap';
import Image from 'next/image'


const _HEADER = (props) => {
    const [isActive, setIsActive] = useState(false)

    const toggleButton = useCallback(
        () => setIsActive(prevState => !prevState),
        [],
    )

    return(
        <header>        
            <Navbar expand={false}>
                <Container>
                    <Navbar.Brand href="/">
                        <div className="next-image">
                            <Image src="/images/icons/logo.svg" width={120} height={0} />
                        </div>                        
                    </Navbar.Brand>
                    <Nav className="justify-content-end flex-row" activeKey="/home">
                        <Nav.Item className="pr-4">
                            <Nav.Link href="/login">
                                <div className="next-image">
                                    <Image src="/images/icons/login.svg" width={35} height={0} />
                                </div>                                
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="pr-3">
                            <Nav.Link href="/home">
                                <div className="next-image">
                                    <Image src="/images/icons/seach.svg" width={35} height={0} />
                                </div>                            
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" className="border-0">
                                <HamburgerSqueeze
                                    barColor="#50d0b2"
                                    buttonWidth="24"
                                    className="pt-3 pr-0 pl-0"
                                    buttonStyle={{ outline: 'none' }}
                                    {...{ isActive, toggleButton }}
                                />
                            </Navbar.Toggle>
                        </Nav.Item>
                    </Nav>
                    <Navbar.Collapse id="basic-navbar-nav" className="position-absolute w-100">
                        <Container>
                            <Nav className="mr-auto w-100 pt-5 pb-5">
                                <Nav.Link href="/ListPostCategorys" className="p-3 text-uppercase text-left">Conta Uzzipay</Nav.Link>
                                <Nav.Link href="/ListPostCategorys" className="p-3 text-uppercase text-left">Outros</Nav.Link>
                                <Nav.Link href="/ListPostCategorys" className="p-3 text-uppercase text-left">Pix</Nav.Link>
                                <Nav.Link href="/ListPostCategorys" className="p-3 text-uppercase text-left">Segurança</Nav.Link>
                                <Nav.Link href="/ListPostCategorys" className="p-3 text-uppercase text-left">Ideias</Nav.Link>
                                <Nav.Link href="/ListPostCategorys" className="p-3 text-uppercase text-left">Cartão de crédito</Nav.Link>
                                <Nav.Link href="/ListPostCategorys" className="p-3 text-uppercase text-left">Comunidade</Nav.Link>
                            </Nav>
                        </Container>
                        <Container>
                            <hr />
                            <Nav className="mr-auto justify-content-between flex-row w-100  pt-2 pb-4">
                                <Nav.Link href="#home" className="p-3 text-uppercase text-center">termos de uso</Nav.Link>
                                <Nav.Link href="#home" className="p-3 text-uppercase text-center">sobre</Nav.Link>
                                <Nav.Link href="#home" className="p-3 text-uppercase text-center">privacidade</Nav.Link>
                            </Nav>
                        </Container>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </header>
    )
}

export default _HEADER