import React from 'react'
import APP from '../layout/App'
import _LISTCATEGORYS from '../components/ListCategory'
import { Container } from 'react-bootstrap'
import _CARDPOST from '../components/CardPost'

export default function Home() {
    return (
        <APP 
            title="Conta UzziPay" 
            description="Descição so site"
            keywords="Palavras Chaves"
            subjet="Subjects or Description"
            abstract="abstract or Description"
            topic="topic or Description"
            summary="summary or Description"
            image="Imagem"
        >
            <section className="posts">
                <Container className="posts-list">
                    <div className="pt-4 pl-2 pl-md-5 pb-4">
                        <h3><strong>Conta da UzziPay</strong></h3>
                    </div>
                    <_CARDPOST />       
                    <_CARDPOST />    
                    <_CARDPOST />    
                    <_CARDPOST />    
                    <_CARDPOST />    
                    <_CARDPOST />    
                    <_CARDPOST />    
                    <_CARDPOST />    
                    <_CARDPOST />    
                </Container>
            </section>
        </APP>
    )
}
