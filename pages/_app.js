import React from 'react'
import App from 'next/app';
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";

import promise from "redux-promise";
import multi from "redux-multi";
import thunk from "redux-thunk";

import Reducers from './api/rootReducer'

import '../styles/app.scss'

class MyApp extends App {

    static async getInitialProps({Component, ctx}) {
        const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
        return {pageProps: pageProps};
    }

    render() {
        const {Component, pageProps} = this.props;
        const devTools =
                global.__REDUX_DEVTOOLS_EXTENSION__ &&
                global.__REDUX_DEVTOOLS_EXTENSION__();
            const store = applyMiddleware(thunk, multi, promise)(createStore)(
                Reducers,
                devTools
            );
        return (
            <Provider store={store}>
                <Component {...pageProps}/>
            </Provider>
        );
    }
}

export default MyApp;