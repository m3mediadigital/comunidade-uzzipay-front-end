import Config from '../config';
const SINGULAR = "CATEGORY";
const PLURAL = "CATEGORYS";

export const unset = () => {
    return {
        type: `UNSET_${SINGULAR}_INFO`
    };
};

export const getCategory = (slug) => {
    return dispatch => {
        dispatch(unset());
        Config.get("categorys-posts", slug, dispatch, `SET_${SINGULAR}`);
    };
};

export const getCategorys = (method) => {
    return dispatch => {
        dispatch(unset());
        Config.list(method, dispatch, `SET_${PLURAL}`);
    };
};