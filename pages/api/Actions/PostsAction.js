import Config from '../config';
const SINGULAR = "POST";
const PLURAL = "POSTS";

export const unset = () => {
    return {
        type: `UNSET_${SINGULAR}_INFO`
    };
};

export const get = (slug) => {
    return dispatch => {
        dispatch(unset());
        Config.get("post", slug, dispatch, `SET_${SINGULAR}`);
    };
};

export const list = () => {
    return dispatch => {
        dispatch(unset());
        Config.list("posts", dispatch, `SET_${PLURAL}`);
    };
};

export const addPosts = (form) => {
    return dispatch => {
        dispatch(unset())
        Config.add('post-store', form, dispatch, `SET_${SINGULAR}`)
    }
}