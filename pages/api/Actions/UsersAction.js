import Config from '../config';
const SINGULAR = "USER";
const PLURAL = "USERS";

export const unset = () => {
    return {
        type: `UNSET_${SINGULAR}_INFO`
    };
};

export const getUser = (slug) => {
    return dispatch => {
        dispatch(unset());
        Config.get("/", slug, dispatch, `SET_${SINGULAR}`);
    };
};

export const getUsers = () => {
    return dispatch => {
        dispatch(unset());
        Config.list("users", dispatch, `SET_${PLURAL}`);
    };
};

export const addUsers = (form) => {
    return dispatch => {
        dispatch(unset());
        Config.add("users/create", form, dispatch, `SET_${SINGULAR}`);
    };
};