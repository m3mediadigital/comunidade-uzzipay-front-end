import { fromJS } from "immutable";

const SINGULAR = "CATEGORY";
const PLURAL = "CATEGORYS";

const INITIAL_STATE = fromJS({
    item_category: null,
    list_category: []
});

const initial = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case `SET_${SINGULAR}`:
            return state.set("item_category", fromJS(action.payload));
        case `SET_${PLURAL}`:
            return state.set("list_category", fromJS(action.payload));
        case `SET_${PLURAL}_META`:
            return state.set("meta", fromJS(action.payload));
        case `UNSET_${SINGULAR}_INFO`:
            return state.set("item_category", fromJS([]));
        case `UNSET_${PLURAL}_INFO`:
            return state.set("list_category", fromJS([]));
        default:
            return state;
    }
};

export default initial