import { fromJS } from "immutable";

const SINGULAR = "POST";
const PLURAL = "POSTS";

const INITIAL_STATE = fromJS({
    get: null,
    list: []
});

const initial = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case `SET_${SINGULAR}`:
            return state.set("get", fromJS(action.payload));
        case `SET_${PLURAL}`:
            return state.set("list", fromJS(action.payload));
        case `SET_${PLURAL}_META`:
            return state.set("meta", fromJS(action.payload));
        case `UNSET_${SINGULAR}_INFO`:
            return state.set("get", fromJS([]));
        case `UNSET_${PLURAL}_INFO`:
            return state.set("list", fromJS([]));
        default:
            return state;
    }
};

export default initial