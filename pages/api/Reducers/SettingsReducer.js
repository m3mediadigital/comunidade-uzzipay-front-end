import { fromJS } from "immutable";

const SINGULAR = "SETTING";
const PLURAL = "SETTINGS";

const INITIAL_STATE = fromJS({
    item_setting: null,
    list_settings: []
});

const initial = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case `SET_${SINGULAR}`:
            return state.set("item_setting", fromJS(action.payload));
        case `SET_${PLURAL}`:
            return state.set("list_settings", fromJS(action.payload));
        case `SET_${PLURAL}_META`:
            return state.set("meta", fromJS(action.payload));
        case `UNSET_${SINGULAR}_INFO`:
            return state.set("item_setting", fromJS([]));
        case `UNSET_${PLURAL}_INFO`:
            return state.set("list_settings", fromJS([]));
        default:
            return state;
    }
};

export default initial