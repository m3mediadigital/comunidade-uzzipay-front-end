import { fromJS } from "immutable";

const SINGULAR = "USER";
const PLURAL = "USERS";

const INITIAL_STATE = fromJS({
    item_user: null,
    list_user: [],
    error_user: [],
    success_user: []
});

const initial = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case `SET_${SINGULAR}`:
            return state.set("item_user", fromJS(action.payload));
        case `SET_${PLURAL}`:
            return state.set("list_user", fromJS(action.payload));

        case `SET_ERROR_${SINGULAR}`:
            return state.set('error_user', fromJS(action.payload))
        case `SET_SUCCESS_${SINGULAR}`:
            return state.set('success_user', fromJS(action.payload))
    
       case `UNSET_${PLURAL}_INFO`:
        return state
            .set('list_user', fromJS([]))
            .set('item_user', fromJS([]))
            .set('error_user', fromJS([]))
            .set('success_user', fromJS([]))
        default:
        return state
    }
};

export default initial