import api from "./api";
import fireError from './fireError'
import fireSuccess from './fireSuccess'
import fireWarning from './fireWarning'

const get = (method, slug, dispatch, singular) => {
    api.get(method + "/" + slug)
        .then(response => {
            dispatch({
                type: singular,
                payload: response.data
            });
        })
        .catch(error => {
            console.log(error);
        });
};

const list = (method, dispatch, plural) => {
    api.get(method)
        .then(response => {
            dispatch({
                type: plural,
                payload: response.data
            });
        })
        .catch(error => {
            console.log(error);
        });
};

const add = (method, form, dispatch, singular) => {
    api.post(method, form,{
        headers:{
            'Content-Type': 'multipart/form-data'
        }
    })
    .then(response => {
        if(response.data.error){
            fireWarning(response.data.error)
        }else if(response.data.success){
            // dispatch({
            //     type: `SET_SUCCESS_${singular}`,
            //     payload: response.data
            // })
             fireSuccess(response.data.success)           
        }
    })
    .catch(error => {
        fireError()
    })
}

export default { get, list, add };