import Swal from "sweetalert2";

export default msg => {
  Swal.fire({
    title: 'Sucesso!',
    icon: 'success',
    text: typeof msg !== 'undefined' ? msg : 'Operação realizada com sucesso.',
    type: 'success'
  })
}
