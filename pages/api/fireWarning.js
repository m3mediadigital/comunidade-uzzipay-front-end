import Swal from "sweetalert2";

export default msg => {
  Swal.fire({
    title: 'Ops!',
    icon: 'warning',
    text: typeof msg !== 'undefined' ? msg : 'Verifique informação.',
    type: 'warning'
  })
}
