import { combineReducers } from 'redux'
import Categorys from './Reducers/CategorysReducer'
import Posts from './Reducers/PostsReducer'
import Settings from './Reducers/SettingsReducer'
import User from './Reducers/UsersReducer'

const rootReducer = combineReducers({
    category: Categorys,
    post: Posts,
    settings: Settings,
    user: User
})

export default rootReducer