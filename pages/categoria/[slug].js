import React from 'react'
import APP from '../../layout/App'
import useRouter from 'next/router'

import { Container } from 'react-bootstrap'
import { getCategory } from '../api/Actions/CategorysAction'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import PropTypes from 'prop-types'
import Immutable from 'immutable'

import _CARDPOST from '../../components/CardPost'
import Loading from '../../components/Loading'


class Category extends React.Component {
    constructor(props) {
        super(props)

        this.getCategory()
    }

    async getCategory(){
        const {query} = useRouter
        this.props.getCategory(query.slug) 
    }

    render() {
        const { item_category } = this.props
        const item = item_category !== null ? item_category.toJS() : ''
        
        return (
            <APP
                title={item.title}
                description="Descição so site"
                keywords="Palavras Chaves"
                subjet="Subjects or Description"
                abstract="abstract or Description"
                topic="topic or Description"
                summary="summary or Description"
                image="Imagem"
            >
                <section className="posts" style={{ minHeight: '79vh' }}>
                    <Container className="posts-list">
                        <div className="pt-4 pl-2 pl-md-5 pb-4">
                            <h3><strong>{item.title}</strong></h3>
                        </div>
                            {
                                item_category !== null && item_category.size > 0 ? (
                                    item.post.map((item, key) => (
                                        <_CARDPOST item={item} key={key} />
                                    ))
                                ): (                                    
                                    <Loading />
                                )
                            } 
                    </Container>
                </section>
            </APP>
        )
    }
}


const mapStateToProps = state => ({
    item_category: state.category.get('item_category')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        getCategory
    }, dispatch)


Category.propTypes = {
    item_category: PropTypes.oneOfType([
        PropTypes.instanceOf(Immutable.List),
        PropTypes.instanceOf(Immutable.Map)
    ]),
    getCategory: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)