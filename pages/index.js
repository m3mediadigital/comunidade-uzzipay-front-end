import React from 'react'
import APP from '../layout/App'

import _POSTS from '../components/postsIndex'
import _LISTCATEGORYS from '../components/ListCategory'

import { getCategorys } from './api/Actions/CategorysAction'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
class Home extends React.Component {

    constructor(props) {
        super(props);
        this.getCategorys()
    }

    getCategorys() {
        this.props.getCategorys('categorys')
    }

    render() {
        return (
            <APP
                title="Comunidade Uzzipay"
                description="Descição so site"
                keywords="Palavras Chaves"
                subjet="Subjects or Description"
                abstract="abstract or Description"
                topic="topic or Description"
                summary="summary or Description"
                image="Imagem"
            >
                <_POSTS />
                <_LISTCATEGORYS categorys={this.props.list_category} />
            </APP>
        )
    }
}

const mapStateToProps = state => ({
    list_category: state.category.get('list_category')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        getCategorys
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Home)