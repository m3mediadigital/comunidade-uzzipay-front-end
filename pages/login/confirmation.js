import { Container } from "react-bootstrap"
import App from "../../layout/App"

const _CONFIRMATION = () =>{
    return(
        <App>
            <Container>
                <h2>Senha Alterada</h2>
            </Container>
        </App>
        
    )
}

export default _CONFIRMATION