import React from 'react';
import Button from '@material-ui/core/Button';
import { Col, Form, Row } from 'react-bootstrap';
import { useRouter } from 'next/router'


export default function _FORMCONFIRM() {
    const router = useRouter()

    return (
        <div className="pt-5 pb-5 d-flex justify-content-center">
            <form noValidate autoComplete="off" className="login-forms d-flex flex-wrap">
                <div className="w-100 pb-3 pt-3">
                    <h1><strong>Quase pronto</strong></h1>
                    <p>Para finalizar o seu cadastro, digite o código de confirme o código enviado para o e-mail cadastrado: <strong>usu*******@uzz****.com.br</strong></p>
                </div>
                <Row>
                    <Col>
                        <Form.Control style={{ height: '70px' }} className="text-center" type="tel" />
                    </Col>
                    <Col>
                        <Form.Control style={{ height: '70px' }} className="text-center" type="tel" />
                    </Col>
                    <Col>
                        <Form.Control style={{ height: '70px' }} className="text-center" type="tel" />
                    </Col>
                    <Col>
                        <Form.Control style={{ height: '70px' }} className="text-center" type="tel" />
                    </Col>
                    <Col>
                        <Form.Control style={{ height: '70px' }} className="text-center" type="tel" />
                    </Col>
                </Row>
                <Button variant="contained" className="w-100 mt-5 mb-4 p-2" onClick={() => router.push('/login/resetPassword')}>Entrar</Button>
            </form>
        </div>
    );
}