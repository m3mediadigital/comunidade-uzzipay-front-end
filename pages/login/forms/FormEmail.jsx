import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Button from '@material-ui/core/Button';
import Link from 'next/link'



const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        margin: theme.spacing(1),
    },
    withoutLabel: {
        marginTop: theme.spacing(3),
    },
    textField: {
        width: '25ch',
    },
}));

export default function _FORMLOGIN() {

    const classes = useStyles();
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const preventDefault = (event) => event.preventDefault();
    
    return (
        <div className="mb-5 w-100 d-flex justify-content-center pt-5">
            <form className={classes.root} noValidate autoComplete="off" className="login-forms pt-5 mb-5">
                <div className="w-100 pb-3 pt-3">
                    <h1 className="text-center">Recupere <strong>sua senha</strong></h1>
                </div>
                <TextField 
                    id="standard-basic" 
                    label="E-mail"
                    variant="filled"
                    className="w-100 mb-3"
                    type="email"
                />
                <Link href="/login/confirmResetPass">
                    <Button variant="contained" className="w-100 mt-4 mb-5 p-2">Continuar</Button>
                </Link>
            </form>
        </div>
    );
}