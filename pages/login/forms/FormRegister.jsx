import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Button from '@material-ui/core/Button';
import { Col, Row } from 'react-bootstrap';

import { addUsers } from '../../api/Actions/UsersAction'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types'
import Immutable from 'immutable'


class _FORMREGISTER extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            cpf: '',
            password_confirmation: '',
            showPassword: false,
            checkedA: false,
            sending: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
      
    };

    handleClickShowPassword = () => {
        this.setState({
            showPassword : this.state.showPassword === false ? true : false
        })
    };

    handleCheck = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    handleSubmit(event){
        event.preventDefault()

        const formData = new FormData()
        formData.append('name', this.state.name)
        formData.append('email', this.state.email)
        formData.append('cpf', this.state.cpf)
        formData.append('password', this.state.password)
        formData.append('password_confirmation', this.state.password_confirmation)
        formData.append('check', this.state.checkedA)

        this.props.addUsers(formData)
    }
        
    render(){
        console.log(this.props)
        return (
            <form className="d-flex flex-wrap" noValidate autoComplete="off" className="login-forms login-forms-register w-100" onSubmit={this.handleSubmit}>
                <div className="w-100 pb-3">
                    <h1><strong>Cadastre-se</strong></h1>
                    <p>Faça o seu cadastro e interaja com nossos diversos <br /> usuários!</p>
                </div>
                <Row>
                    <Col sm={12}>
                        <TextField 
                            id="standard-basic" 
                            label="Nome"
                            variant="filled"
                            className="w-100 mb-3"
                            name="name"
                            onChange={this.handleChange}
                        />
                    </Col>
                    <Col sm={12}>
                        <TextField 
                            id="standard-basic" 
                            label="E-mail"
                            variant="filled"
                            className="w-100 mb-3"
                            type="email"
                            name="email"
                            onChange={this.handleChange}
                        />
                    </Col>
                    <Col sm={12}>
                        <TextField 
                            id="standard-basic" 
                            label="CPF"
                            variant="filled"
                            className="w-100 mb-3"
                            type="number"
                            name="cpf"
                            onChange={this.handleChange}
                        />
                    </Col>
                    <Col sm={12} md={6}>
                        <FormControl className="w-100" >
                            <InputLabel className="label" htmlFor="standard-adornment-password">Senha</InputLabel>
                            <Input
                                id="standard-adornment-password"
                                type={this.state.showPassword ? 'text' : 'password'}
                                value={this.state.password}
                                variant="filled"
                                onChange={this.handleChange}
                                name="password"
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={this.handleClickShowPassword}
                                        onMouseDown={this.handleMouseDownPassword}
                                        >
                                        {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Col>
                    <Col sm={12} md={6}>
                        <TextField 
                            id="standard-basic" 
                            label="Repetir senha"
                            variant="filled"
                            className="w-100 mb-3"
                            type="password"
                            name="password_confirmation"
                            onChange={this.handleChange}
                        />
                    </Col>
                    <Col sm={12} md={6} className="d-flex align-items-center">
                        <FormControlLabel className="label-checkbox"
                            control={<Checkbox checked={this.state.checkedA} onChange={this.handleChange} name="checkedA" value="true" />}
                            label="Concordo com os termos e condições"
                        />
                    </Col>
                    <Col sm={12} md={6}>
                        <Button variant="contained" type="submit" className="w-100 mt-4 mb-1 p-2">Cadastrar</Button>
                    </Col>
                </Row>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    // user: state.user.get('user'),
    error: state.user.get('error_user')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        addUsers
    }, dispatch)


_FORMREGISTER.propTypes = {
    error: PropTypes.oneOfType([
        PropTypes.instanceOf(Immutable.List),
        PropTypes.instanceOf(Immutable.Map)
    ]),
    error: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(_FORMREGISTER)