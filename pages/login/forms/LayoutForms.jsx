import React, {Component} from 'react'

import { getSettings } from '../../api/Actions/SettingsAction'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class LayoutForms extends Component{

    constructor(props) {
        super(props);
        this.getSettings()
    }

    getSettings(){
        this.props.getSettings()
    }

    render(){
        return(
            <React.Fragment>
                {this.props.children}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    list_settings: state.settings.get('list_settings')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        getSettings
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(LayoutForms)