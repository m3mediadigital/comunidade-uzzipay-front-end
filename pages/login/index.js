import { Col, Container, Row, Card } from 'react-bootstrap';

import APP from '../../layout/App'
import _FORMLOGIN from './forms/FormLogin'

export default function Login() {
    return(
        <APP
            title="Login" 
            description="Descição so site"
            keywords="Palavras Chaves"
            subjet="Subjects or Description"
            abstract="abstract or Description"
            topic="topic or Description"
            summary="summary or Description"
            image="Imagem"
        >
            <section className="login">
                <Container>
                    <Row>
                        <Col sm={12} lg={6} className="d-none d-lg-block">
                            <Card className="w-100 border-0 posts-card posts-card-one" style={{ backgroundImage: 'url(images/postOne.png)' }}>
                                <Card.Body className="p-5 w-100">
                                    <div className="card-body-content position-absolute pt-5">
                                        <Card.Title className="w-75 pb-3 pt-5">
                                            <strong>Comunidade</strong>
                                            <br />
                                            <strong className="text-green">Uzzipay!</strong>
                                        </Card.Title>
                                        <Card.Text className="pb-3">
                                            A comunidade da Uzzipay é um espaço ideal para tirar dúvidas, dar sugestões e trocar ideias sobre sustentabilidade.
                                        </Card.Text>
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col sm={12} lg={6} className="d-flex justify-content-center align-items-center">
                            <_FORMLOGIN />
                        </Col>
                    </Row>
                </Container>
            </section>        
        </APP>
    )
}