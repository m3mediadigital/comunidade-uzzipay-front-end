import APP from '../../layout/App'
import {Row, Col, Container } from 'react-bootstrap'

import _HEADER from '../../components/m3/Header'
import _CARDPOST from '../../components/m3/CardPost'
export default function _INDEX() {
    return (
        <APP 
            title="Seja Bem Vindo" 
            description="Descição so site"
            keywords="Palavras Chaves"
            subjet="Subjects or Description"
            abstract="abstract or Description"
            topic="topic or Description"
            summary="summary or Description"
            image="Imagem"
        >
            <_HEADER />
            <section className="posts pt-3">
                <Container className="posts-list">
                    <div className="pt-4 pl-0 pb-4">
                        <h3><strong>Postagens</strong></h3>
                    </div>
                    <Row>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                        <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                         <Col sm={12} md={6} className="pb-4">
                            <_CARDPOST /> 
                        </Col>
                    </Row>  
                </Container>
            </section>
        </APP>
    )
}
