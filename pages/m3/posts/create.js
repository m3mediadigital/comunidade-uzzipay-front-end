import React from 'react'
import APP from '../../../layout/App'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import CKEditor from 'ckeditor4-react';

import { getCategorys } from '../../api/Actions/CategorysAction'
import { addPosts } from '../../api/Actions/PostsAction'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import fireWarning from '../../api/fireWarning'


class Create extends React.Component {

    constructor(props) {
        super(props);
        this.getCategorys()

        this.state = {
            title: '',
            category_id: '',
            content: '',
            file: '',
            link: ''
        }

        this.handleFiles = this.handleFiles.bind(this)
        this.handleCK = this.handleCK.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    getCategorys() {
        this.props.getCategorys('post-categorys')
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    handleCK = (event) => {
        this.setState({
            content: event.editor.getData()
        })
    }

    handleFiles(event){
        this.setState({
            file: event.target.files[0]
        })
    }

    handleSubmit(event){
        event.preventDefault()
        console.log(this.state)
        const formData = new FormData()
        formData.append('title', this.state.title)
        formData.append('category_id', this.state.category_id)
        formData.append('content', this.state.content)
        formData.append('file', this.state.file)
        formData.append('link', this.state.link)

        this.props.addPosts(formData)
    }

    render(){
        const {list_category} = this.props
        return(
            <APP>
                <Container>
                    <div className="pt-5 pb-4">
                        <h3><strong>Nova Postagem</strong></h3>
                    </div>
                    <div className="hr pt-4 pb-4">
                        <p>Todos os campo com <em className="text-danger">*</em> são obrigatórios!</p>
                        <hr />
                    </div>
                    <Form class="posts-form" onSubmit={this.handleSubmit}>
                        <Row>
                            <Col sm={12} md={12}>
                                <Form.Group>
                                    <Form.Label>Título <em className="text-danger">*</em> </Form.Label>
                                    <Form.Control type="text" placeholder="Título aqui" className="rounded-0" required name="title" onChange={this.handleChange}/>
                                </Form.Group>
                            </Col>
                            <Col sm={12} md={6}>
                                <Form.Group>
                                    <Form.Label>Categoria <em className="text-danger">*</em> </Form.Label>
                                    <Form.Control as="select" name="category_id" className="rounded-0" required onChange={this.handleChange}>
                                        <option value="">Selecione Categoria</option>
                                        {
                                            list_category.map((item, key) => (
                                                <option value={item.get('id')} key={key}>{item.get('title')}</option>
                                            ))
                                        }
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col sm={12} md={6}>
                                <Form.Group>
                                    <Form.File id="formcheck-api-regular">
                                        <Form.File.Label>Imagem <em className="text-danger">*</em> </Form.File.Label>
                                        <Form.File.Input name="file" onChange={this.handleFiles} required/>
                                    </Form.File>
                                </Form.Group>
                            </Col>
                            <Col sm={12}>
                                <Form.Group>
                                    <Form.Label>Link</Form.Label>
                                    <Form.Control type="url" name="link" className="rounded-0" onChange={this.handleChange} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Form.Group>
                            <Form.Label>Conteúdo <em className="text-danger">*</em></Form.Label>
                            <CKEditor
                                onChange={this.handleCK}
                                config={{         
                                    toolbar: [['Bold', 'Italic', 'BlockQuote', 'Link', 'NumberedList', 'BulletedList', '|', 'Undo', 'Redo']],
                                    height:  300
                                }}
                            />  
                        </Form.Group>
                        <Col sm={12} className="text-right pr-0 pt-3">
                            <Button type="submit" variant="success" className="rounded-0 pl-3 pr-3">Postar</Button>
                        </Col>
                    </Form>
                </Container>
            </APP>
        )
    }
}

const mapStateToProps = state => ({
    list_category: state.category.get('list_category')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        getCategorys,
        addPosts
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Create)
