import APP from '../../layout/App'
import { Container } from 'react-bootstrap'

import _HEADER from '../../components/m3/Header'
import _CARDPOST from '../../components/m3/CardPost'
import _FORMPREFERENCES from '../../components/m3/FormPreferences'
export default function _INDEX() {
    return (
        <APP 
            title="Preferências" 
            description="Descição so site"
            keywords="Palavras Chaves"
            subjet="Subjects or Description"
            abstract="abstract or Description"
            topic="topic or Description"
            summary="summary or Description"
            image="Imagem"
        >
            {/* <_HEADER /> */}
            <section className="posts pt-3">
                <Container className="posts-list">
                    <div className="pt-4 pl-0 pb-4">
                        <h3><strong>Preferências</strong></h3>
                    </div>
                    <_FORMPREFERENCES />
                </Container>
            </section>
        </APP>
    )
}
