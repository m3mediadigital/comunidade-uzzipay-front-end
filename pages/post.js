import APP from '../layout/App'
import _LISTCATEGORYS from '../components/ListCategory'
import { Container, Image } from 'react-bootstrap'
import _CARDPOST from '../components/CardPost'
import Like from '../components/likes'
import _COMMENTS from '../components/Commnets'

export default function Home() {
    return (
        <APP 
            title="Conta UzziPay" 
            description="Descição so site"
            keywords="Palavras Chaves"
            subjet="Subjects or Description"
            abstract="abstract or Description"
            topic="topic or Description"
            summary="summary or Description"
            image="Imagem"
        >
            <section className="posts">
                <Container className="posts-intern">
                    <div className="pt-5 pb-4">
                        <h3><strong>Como faço para abrir a minha conta?</strong></h3>
                    </div>
                    <Like />
                    <div className="hr pt-4 pb-4">
                        <hr />
                    </div>
                    <figure className="figure">
                        <Image bsPrefix="img" src="images/post.jpg" className="img-fluid" />
                        <figcaption className="figure-caption pt-1">
                            <i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</i>
                        </figcaption>
                    </figure>
                    <div className="posts-intern-text pt-5">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text 
                            ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has 
                            survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised 
                            in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software 
                            like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, 
                            making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more 
                            obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, 
                            discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” 
                            (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the 
                            Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.
                        </p>
                    </div>
                    <div className="hr pt-4 pb-4">
                        <hr />
                    </div>
                    <_COMMENTS />
                    <div className="hr pt-4 pb-4">
                        <hr />
                    </div>
                    <div className="posts">
                        <div className="posts-list">
                            <p className="pb-3">Veja também</p>
                            <_CARDPOST />       
                            <_CARDPOST />
                        </div>
                    </div>
                </Container>
            </section>
        </APP>
    )
}
