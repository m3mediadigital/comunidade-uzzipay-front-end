import React from 'react'

import useRouter from 'next/router'
import { get } from '../api/Actions/PostsAction'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types'
import Immutable from 'immutable'

import _POST from '../../components/post'
import Loading from '../../components/Loading'
import APP from '../../layout/App'

class Post extends React.Component{
    constructor(props){
        super(props)

        this.get()
    }

    async get()
    {
        const {query} = useRouter
        this.props.get(query.slug)
    }

    render(){
        const { post } = this.props
        const item = post !== null ? post.toJS() : ''

        return (
            <APP 
            title={item.title} 
            description={item.content}
            keywords="Palavras Chaves"
            subjet="Subjects or Description"
            abstract="abstract or Description"
            topic="topic or Description"
            summary="summary or Description"
            image="Imagem"
        >
                {
                    post !== null && post.size > 0 ? (
                        <_POST item={item} />
                    ) : (
                        <Loading />
                    )
                        
                }
            </APP>
        )
    }
}

const mapStateToProps = state => ({
    post: state.post.get('get')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        get
    }, dispatch)


Post.propTypes = {
    get: PropTypes.oneOfType([
        PropTypes.instanceOf(Immutable.List),
        PropTypes.instanceOf(Immutable.Map)
    ]),
    get: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)