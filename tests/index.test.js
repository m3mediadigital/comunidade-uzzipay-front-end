import { render, screen } from "@testing-library/react";
import _index from "../pages/index";

describe("_index", () => {
    it("renders without crashing", () => {
        render(<_index />);
        expect(
            screen.getByRole("heading", { name: "Welcome to Next.js!" })
        ).toBeInTheDocument();
    });
});